import scipy.optimize as opt
import time
import sys
sys.path.insert(0,'..')
from Neural_Networks import *
from Utils import *

#-------------------- OPEN DATASET --------------------#

dataset = np.genfromtxt("../DATA.csv", delimiter=',')
X_train, X_test, y_train, y_test = get_train_test_set_from_data(dataset)

final_err = run_me(X_train, y_train)

#-------------------- Printing Values --------------------#

print("Final error:", final_err)

