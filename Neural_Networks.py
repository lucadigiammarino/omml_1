import numpy as np
from Utils import *

#----------------- TANH FUNCTION (allows to set sigma) ---------------#

def g(x, sigma):
    N = 1 - np.exp(-sigma*2*x)
    D = 1 + np.exp(-sigma*2*x)
    return N/D

#----------------- GAUSSIAN FUNCTION (allows to set sigma) ---------------#

def gaussian_function(x, sigma):
    return np.exp(-((x/sigma)**2))

#----------------- Shallow MLP network  ---------------#

def mlp_network(X_train, V, W, B, N, sigma, P):
    V = V.reshape(N, 1)
    B = np.tile(B, (P, 1))
    a_output = X_train.dot(W) - B
    z_output = g(a_output, sigma = sigma)
    y_net = z_output.dot(V)
    return y_net

#----------------- RBF network ---------------#

def rbf_network(X_train, omega, N, sigma, P):
    y_output = np.zeros(P)

    # Split omega into V, C
    Vt    = omega[:N]
    C_vec = omega[N:]

    V = Vt.reshape(1, N)
    C = C_vec.reshape((2, N))
    C = np.tile(C, (P, 1, 1))

    a_output = np.zeros((N, P))
    z_output = np.zeros((N, P))

    for n in range(N):
        a_output[n] = np.linalg.norm(X_train - C[:, :, n], 2, axis=1)
        z_output[n] = gaussian_function(a_output[n], sigma)

    y_net = V.dot(z_output)

    return np.transpose(y_net)

#----------------- RBF network (unsupervised selection of centers) ---------------#

def rbf_network_unsup(X_train, V, C, N, sigma, P):

    C = np.tile(C, (P, 1, 1)) # matrix of centers for the hidden layer set as random values

    a_output = np.zeros((N, P))
    z_output = np.zeros((N, P))

    for n in range(N):
        a_output[n, :] = np.linalg.norm(X_train - C[:, :, n], 2, axis = 1)
        z_output[n, :] = gaussian_function(a_output[n, :], sigma)

    y_net = V.dot(z_output)

    return np.transpose(y_net)

#-------------------- Regularized Training Error (optimize V)--------------------#
def reg_trai_err_V(V, W, B, net_input, y_expected, N, sigma, rho, P, isTraining):
    y_obtained = mlp_network(net_input, V, W, B, N, sigma, P)
    res = 0.
    for p in range(P):
        res += np.square(y_obtained[p] - y_expected[p])

    if isTraining:
        error = (1 / (2 * P)) * res + (rho * np.linalg.norm(V, 2))
    else:
        error = (1 / (2 * P)) * res

    return error

#-------------------- Regularized Training Error (optimize W and B)--------------------#

def reg_trai_err_omega(omega, V, net_input, y_expected, N, sigma, rho, P, isTraining):
    # Split omega into W, V, B

    if (len(omega)==4*N):
        V, W, B = slice_omega(omega, N)
    else:
        W_vec = omega[:2 * N]
        B = omega[2 * N:]
        W = np.reshape(W_vec, (2, N))
        B = B.reshape(N, 1)
        B = np.transpose(B)

    y_obtained = mlp_network(net_input, V, W, B, N, sigma, P)
    res = 0.
    for p in range(P):
        res += np.square(y_obtained[p] - y_expected[p])

    if isTraining:
        error = (1 / (2 * P)) * res + (rho * np.linalg.norm(omega, 2))
    else:
        error = (1 / (2 * P)) * res

    return error

#-----------------Regularized Training Error RBF ----------------------------#

def reg_trai_err_RBF(omega, net_input, y_expected, N, sigma, rho, P, isTraining):
    y_obtained = rbf_network(net_input, omega, N, sigma, P)
    res = 0.
    for p in range(P):
        res += np.square(y_obtained[p] - y_expected[p])

    if isTraining:
        error = (1 / (2 * P)) * res + (rho * np.linalg.norm(omega, 2))
    else:
        error = (1 / (2 * P)) * res

    return error

#-----------------Regularized Training Error RBF (unsupervised selection of centers) ----------------------------#

def reg_trai_err_RBFunsup(V, C, net_input, y_expected, N, sigma, rho, P, isTraining):
    y_obtained = rbf_network_unsup(net_input, V, C, N, sigma, P)
    res = 0.
    for p in range(P):
        res += np.square(y_obtained[p] - y_expected[p])

    if isTraining:
        error = (1 / (2 * P)) * res + (rho * np.linalg.norm(V, 2))
    else:
        error = (1 / (2 * P)) * res

    return error

#-----------------BEST NETWORK: MLP with 2-BlockDecomposition ----------------------------#

def run_me(X, Y):

    #-------------------- PARAMETERS --------------------#

    np.random.seed(1822138)           # set seed for random initialization

    isTraining = False                # flag for training or testing
    N          = 37                   # neurons
    sigma      = 1                    # standard deviation
    rho        = 5*(10 ** -4)         # regularization term
    P          = len(X)               # size of training set
    W          = np.random.rand(2, N) # weights initially set as random values
    B          = np.random.rand(1, N) # bias initially set as random values
    V          = np.random.rand(1, N) # bias for the output layer set as random values (transpose, row vector)
    Vt         = np.transpose(V)      # bias for the output layer set as random values (column vector)

    optimization_solver_hidden = "L-BFGS-B"
    #optimization_solver = "CG"
    optimization_solver_output = "BFGS"     # least error obtained with this
    num_function_evaluation = 0

    #-------------------- EVALUATION --------------------#

    init_train_err = reg_trai_err_V(V, W, B, X, Y, N, sigma, rho, P, False)

    total_time_start  = time.time()
    hidden_time_start = time.time()

    previous_error = init_train_err

    W_vector  = np.reshape(W, (2*N,1))           # transpose the W matrix into a vector
    B         = B.reshape(N, 1)
    omega     = np.concatenate([W_vector, B], 0) # omega = (B, W)

    result_WB     = opt.minimize(reg_trai_err_omega, omega, args = (V, X, Y, N, sigma, rho, P, True), method = optimization_solver_hidden)
    WB_final      = result_WB.x

    hidden_time_end = time.time()

    W_vec = WB_final[:2*N]
    B     = WB_final[2*N:]
    W     = np.reshape(W_vec, (2, N))
    B     = B.reshape(N, 1)
    B     = np.transpose(B)

    output_time_start = time.time()

    result_V      = opt.minimize(reg_trai_err_V, V, args = (W, B, X, Y, N, sigma, rho, P, True), method = optimization_solver_output)
    V_final       = result_V.x

    output_time_end = time.time()

    num_function_evaluation += result_V.nfev + result_WB.nfev

    final_train_err = reg_trai_err_V(V_final, W, B, X, Y, N, sigma, rho, P, False)

    total_time_end    = time.time()

    Z_predicted = mlp_network(X, V_final, W, B, N, sigma, P)

    #surface_plot(X[:,0], X[:,1], np.ravel(Z_predicted), Y[:,0], P, sys.argv[0])

    return final_train_err[0]