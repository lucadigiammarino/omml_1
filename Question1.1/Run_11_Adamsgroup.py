import scipy.optimize as opt
import time
import sys
import matplotlib.pyplot as plt
sys.path.insert(0,'..')
from Neural_Networks import *
from Utils import *

#-------------------- PARAMETERS --------------------#

dataset = np.genfromtxt("../DATA.csv", delimiter=',')
X_train, X_test, y_train, y_test = get_train_test_set_from_data(dataset)


np.random.seed(1811286)                           # set seed for random initialization
isTraining = False                                # flag for training or testing
N          = 13                                   # neurons
sigma      = 1                                    # standard deviation
rho        = 0.0005                               # regularization term
P          = len(X_train)                         # size of training set
P_test     = len(X_test)                          # size of test set
W          = np.random.rand(N, 2)                 # weights initially set as random values
B          = np.random.rand(N, 1)                 # bias initially set as random values
V          = np.random.rand(1, N)                 # bias for the output layer set as random values (transpose, row vector)
Vt         = np.transpose(V)                      # bias for the output layer set as random values (column vector)
W_vector   = np.reshape(W, (2*N,1))               # transpose the W matrix into a vector
omega_vec  = np.concatenate([Vt, W_vector, B], 0) # omega = (V, B, W)

optimization_solver = "L-BFGS-B"
#optimization_solver = "CG"
#optimization_solver = "BFGS" # least error obtained with this
#optimization_solver = "TNC"
#optimization_solver = "SLSQP"


#-------------------- MAIN --------------------#

init_train_err = reg_trai_err_omega(omega_vec, V, X_train, y_train, N, sigma, rho, P, False)

time_start  = time.time()
result      = opt.minimize(reg_trai_err_omega, omega_vec, args = (V, X_train, y_train, N, sigma, rho, P, True), method = optimization_solver)
omega_final = result.x
time_end    = time.time()

final_train_err = reg_trai_err_omega(omega_final, V, X_train, y_train, N, sigma, rho, P, False)
test_train_err  = reg_trai_err_omega(omega_final, V, X_test, y_test, N, sigma, rho, P_test, False)


#-------------------- Printing Values --------------------#
print("#######################################################")
print("Number of neurons N:", N)
print("Initial training error:", float(init_train_err[0]))
print("Final training error:", final_train_err[0])
print("Final test error:", test_train_err[0])
print("Optimization solver chosen:", optimization_solver)
print("Norm of the gradient at the optimal point:", np.linalg.norm(result.jac))
print("Time for optimizing the network:", time_end-time_start)
print("Number of function evaluation:", result.nfev)
print("Value of sigma:", sigma)
print("Value of rho:", rho)


V, W, B     = slice_omega(omega_final, N)
Z_predicted = mlp_network(X_train, V, W, B, N, sigma, P)

surface_plot(X_train[:,0], X_train[:,1], np.ravel(Z_predicted), y_train[:,0], P, sys.argv[0])
