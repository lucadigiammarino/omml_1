import scipy.optimize as opt
import time
import sys
sys.path.insert(0,'..')
from Neural_Networks import *
from Utils import *

#-------------------- OPEN DATASET --------------------#

dataset = np.genfromtxt("../DATA.csv", delimiter=',')
X_train, X_test, y_train, y_test = get_train_test_set_from_data(dataset)

#-------------------- PARAMETERS --------------------#

np.random.seed(1822138)                           # set seed for random initialization
isTraining = False                                # flag for training or testing
N          = 37                                   # neurons
sigma      = 1.25                                    # standard deviation
rho        = 5*(10 ** -4)                             # regularization term
P          = len(X_train)                         # size of training set
P_test     = len(X_test)                          # size of test set
W          = np.random.rand(2, N)                 # weights initially set as random values
B          = np.random.rand(1, N)                 # bias initially set as random values
V          = np.random.rand(1, N)                 # bias for the output layer set as random values (transpose, row vector)
Vt         = np.transpose(V)                      # bias for the output layer set as random values (column vector)

#optimization_solver = "L-BFGS-B"
#optimization_solver = "Nelder-Mead"
#optimization_solver = "Powell"
#optimization_solver = "CG"
optimization_solver = "BFGS" # least error obtained with this
#optimization_solver = "TNC"
#optimization_solver = "COBYLA"
#optimization_solver = "SLSQP"


#-------------------- MAIN --------------------#

init_train_err = reg_trai_err_V(V, W, B, X_train, y_train, N, sigma, rho, P, False)

time_start  = time.time()
result      = opt.minimize(reg_trai_err_V, V, args = (W, B, X_train, y_train, N, sigma, rho, P, True), method = optimization_solver)
V_final     = result.x
time_end    = time.time()

final_train_err = reg_trai_err_V(V_final, W, B, X_train, y_train, N, sigma, rho, P, True)
test_train_err  = reg_trai_err_V(V_final, W, B, X_test, y_test, N, sigma, rho, P_test, False)

#-------------------- Printing Values --------------------#

print("Number of neurons N:", N)
print("Initial training error:", float(init_train_err[0]))
print("Final training error:", final_train_err[0])
print("Final test error:", test_train_err[0])
print("Optimization solver chosen:", optimization_solver)
print("Norm of the gradient at the optimal point:", np.linalg.norm(result.jac))
print("Time for optimizing the network:", time_end-time_start)
print("Number of function evaluation:", result.nfev)
print("Value of sigma:", sigma)
print("Value of rho:", rho)

Z_predicted = mlp_network(X_train, V_final, W, B, N, sigma, P)

surface_plot(X_train[:,0], X_train[:,1], np.ravel(Z_predicted), y_train[:,0], P, sys.argv[0])
