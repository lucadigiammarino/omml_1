import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
import time
import scipy.optimize as opt
import sys

#--------------- MODELLING DATASET ---------------#

def get_train_test_set_from_data(dataset):

    X = np.array(dataset[1:, :2])
    y = np.array(dataset[1:, 2])
    y = y.reshape((np.shape(y)[0], 1))

    # splitting the dataset
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=1803850)
    return X_train, X_test, y_train, y_test

def slice_omega(omega, N):
    
    Vt = omega[:N]
    W_vec = omega[N:3 * N]
    B = omega[3 * N:]
    W = np.reshape(W_vec, (2, N))
    B = B.reshape(N, 1)
    B = np.transpose(B)

    return Vt, W, B

def surface_plot(X, Y, Z_predicted, Z_true, P, filename, **kwargs):
    xlabel, ylabel, zlabel = kwargs.get('xlabel',""), kwargs.get('ylabel',""), kwargs.get('zlabel',"")
    fig = plt.figure(filename)

    fig.patch.set_facecolor('white')
    ax = fig.add_subplot(111, projection = '3d')

    ax.scatter(X,Y,Z_predicted, color="red", alpha = 1)
    ax.plot_trisurf(X,Y,Z_predicted, color="red", alpha = 0.8)
    ax.plot_trisurf(X,Y,Z_true, color = "green", alpha = 0.4)

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    plt.show()
    plt.close()